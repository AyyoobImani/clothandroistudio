package ir.hisis.cloth;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.squareup.okhttp.Response;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import ir.hisis.cloth.Utils.Utils;
import ir.hisis.cloth.rest.RestCallback;
import ir.hisis.cloth.rest.RestClient;
import ir.hisis.cloth.rest.model.Image;
import ir.hisis.cloth.rest.model.RestError;
import ir.hisis.cloth.rest.service.ClothServer;

/**
 * Created by pooyafayyaz on 4/26/2015.
 */ public class ImageAdapter extends BaseAdapter {

    private int viewSize;
    private Context mContext;
    public String TAG = ImageAdapter.class.toString();
    Activity activity;
    ArrayList<Image> images ;
    public ImageAdapter(Activity a) {
        activity = a;
        mContext = activity.getApplicationContext();
        initImages();
    }

    private void initImages(){
        ClothServer cs = new RestClient().getClothServer();
        cs.getImages(viewSize, 0, false, false, new RestCallback<Response>() {
            @Override
            public void failure(RestError restError) {
                Log.i(TAG, "oh it failed");
            }

            @Override
            public void success(Response response, retrofit.client.Response response2) {
                response2.getBody();
            }
        });
    }

    public int getCount() {
        return mThumbIds.length;
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    // create a new ImageView for each item referenced by the Adapter
    public View getView(int position, View convertView, ViewGroup parent) {
        View imageItem;
        if (convertView == null) {
            LayoutInflater inflater = activity.getLayoutInflater();
            imageItem = inflater.inflate(R.layout.image_item, null);

            ImageItemViewHolder tag = new ImageItemViewHolder();
            tag.image = (ImageView) imageItem.findViewById(R.id.image);
            tag.tagsContainer = (LinearLayout) imageItem.findViewById(R.id.tags_container);
            tag.likeImage = (ImageView) imageItem.findViewById(R.id.like_image);
            setShareLongClickListenerForImage(tag.image);

            imageItem.setTag(tag);
        } else {
            imageItem =  convertView;
        }

        ImageItemViewHolder tag = (ImageItemViewHolder) imageItem.getTag();
        ImageView img = tag.image;
        img.setImageResource(mThumbIds[position]);

        return imageItem;
    }

    private void setShareLongClickListenerForImage(ImageView image){
        image.setOnLongClickListener(new View.OnLongClickListener() {

            @Override
            public boolean onLongClick(View arg0) {
                try{
                    Log.wtf(TAG, "starting to share image");
                    Utils.shareImage((ImageView) arg0, activity);
                }
                catch(FileNotFoundException e){
                    Toast.makeText(arg0.getContext(), activity.getString(R.string.external_storage_is_uavailable), Toast.LENGTH_LONG).show();
                }catch (IOException e) {
                    Toast.makeText(arg0.getContext(), activity.getString(R.string.proble_with_external_storage), Toast.LENGTH_LONG).show();
                }

                return true;
            }
        });
    }


    // references to our images
    private Integer[] mThumbIds = {
            R.drawable.logo_default,
            R.drawable.sample_2, R.drawable.sample_3,
            R.drawable.sample_4, R.drawable.sample_5,
            R.drawable.sample_6, R.drawable.sample_7,
            R.drawable.sample_0, R.drawable.sample_1,
            R.drawable.sample_2, R.drawable.sample_3,
            R.drawable.sample_4, R.drawable.sample_5,
            R.drawable.sample_6, R.drawable.sample_7,
            R.drawable.sample_0, R.drawable.sample_1,
            R.drawable.sample_2, R.drawable.sample_3,
            R.drawable.sample_4, R.drawable.sample_5,
            R.drawable.sample_6, R.drawable.sample_7

    };

    public static class ImageItemViewHolder{
        public ImageView image;
        public LinearLayout tagsContainer;
        public ImageView likeImage;
    }

}

