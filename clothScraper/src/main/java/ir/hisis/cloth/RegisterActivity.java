package ir.hisis.cloth;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.okhttp.internal.Util;

import ir.hisis.cloth.Utils.UtilUser;
import ir.hisis.cloth.Utils.UtilUser.PasswordMatchException;
import ir.hisis.cloth.rest.RestCallback;
import ir.hisis.cloth.rest.model.RestError;
import ir.hisis.cloth.uiapptemplate.view.FloatLabeledEditText;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class RegisterActivity extends ActionBarActivity {
	
	private static String TAG = RegisterActivity.class.toString();

    TextView registerBtn;
	FloatLabeledEditText userNameETxt;
    FloatLabeledEditText passwordETxt;
    FloatLabeledEditText repeatPasswordETxt;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);

        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();

        setContentView(R.layout.activity_register);
		userNameETxt = (FloatLabeledEditText) findViewById(R.id.edtTxtUserNameRegister);
		passwordETxt = (FloatLabeledEditText) findViewById(R.id.edtTxtPasswordRegister);
		repeatPasswordETxt = (FloatLabeledEditText) findViewById(R.id.edtTxtRepeatPassword);
		registerBtn = (TextView) findViewById(R.id.btnRegister);
		registerBtn.setOnClickListener(new RegBtnClickListener());

        authenticateUser();
	}

	public class RegBtnClickListener implements View.OnClickListener{
		@Override
		public void onClick(View arg0) {
			try {
				UtilUser.registerUser(userNameETxt.getText().toString(),passwordETxt.getText().toString(),
                        repeatPasswordETxt.getText().toString(), getApplication(), new RestCallback<Boolean>() {
                            @Override
                            public void success(Boolean aBoolean, Response response) {
                                authenticateUser();
                            }

                            @Override
                            public void failure(RestError error) {
                                Toast.makeText(getApplicationContext(), error.getStrMessage(), Toast.LENGTH_SHORT).show();
                            }
                        });
			} catch (PasswordMatchException e) {
				Toast.makeText(RegisterActivity.this, R.string.passwords_doesnt_match, Toast.LENGTH_SHORT).show();
				return;
			}
		}
	}


    private void authenticateUser(){
        if(UtilUser.isAuthenticated()){
            finish();
        }
    }
}
