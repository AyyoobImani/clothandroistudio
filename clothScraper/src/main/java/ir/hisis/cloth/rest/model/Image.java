package ir.hisis.cloth.rest.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by vandermonde on 6/13/2015.
 */
public class Image {
    @SerializedName("URL")
    String url;

    @SerializedName("isLiked")
    boolean isLiked;


    @SerializedName("likeCount")
    int likeCount;

    @SerializedName("ID")
    int id;

    @SerializedName("tags")
    List<String> tags;

    @SerializedName("position")
    int location;

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public boolean getIsLiked() {
        return isLiked;
    }

    public void setIsLiked(boolean isLiked) {
        this.isLiked = isLiked;
    }

    public int getLikeCount() {
        return likeCount;
    }

    public void setLikeCount(int likeCount) {
        this.likeCount = likeCount;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

}
