package ir.hisis.cloth.rest;

import android.util.Log;

import ir.hisis.cloth.rest.service.ClothServer;
import retrofit.RestAdapter;

/**
 * Created by vandermonde on 5/28/2015.
 */
public class RestClient {
    public static final String TAG = RestClient.class.toString();
    public static final String BASE_URL = "http://91.98.103.222:8080";
    private ClothServer clothServer;

    public RestClient()
    {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint(BASE_URL)
                .build();
        clothServer = restAdapter.create(ClothServer.class);
    }

    public ClothServer getClothServer()
    {
        return clothServer;

    }

}
