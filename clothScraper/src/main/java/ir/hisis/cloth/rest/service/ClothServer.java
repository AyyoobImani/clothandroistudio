package ir.hisis.cloth.rest.service;

import com.squareup.okhttp.Response;

import ir.hisis.cloth.rest.RestCallback;
import ir.hisis.cloth.rest.model.Message;
import ir.hisis.cloth.rest.model.Register;
import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Query;

/**
 * Created by vandermonde on 5/28/2015.
 */
public interface ClothServer {

    @POST("/register/")
    public void register(@Body Register register, Callback<Message> responseCallback);


    @GET("/rest/images/")
    public void getImages(@Query("viewSize")int viewSize, @Query("viewIndex")int viewIndex, @Query("isMostPopular")boolean isMostPopular,
                             @Query("isUserLiked")boolean isUserLiked,Callback<Response> images);

    @POST("/login")
    public void login(@Body Register register, Callback<Message> callback);

}
