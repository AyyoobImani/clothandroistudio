package ir.hisis.cloth;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import ir.hisis.cloth.Utils.UtilUser;
import ir.hisis.cloth.rest.RestCallback;
import ir.hisis.cloth.rest.model.RestError;
import ir.hisis.cloth.uiapptemplate.view.FloatLabeledEditText;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class LogInPageActivity extends Activity implements OnClickListener {

    private static final String TAG = LogInPageActivity.class.toString();
	public static final String LOGIN_PAGE_AND_LOADERS_CATEGORY = "ir.hisis.cloth.LogInPageAndLoadersActivity";
	public static final String DARK = "Dark";
	public static final String LIGHT = "Light";
    TextView login, register, forgot;
    FloatLabeledEditText UserText,PassText;


    @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		String category = LIGHT;
        setContentView(R.layout.activity_login_page_light);
        UserText=(FloatLabeledEditText) findViewById(R.id.UserNameText);
        PassText=(FloatLabeledEditText) findViewById(R.id.PassText);
        login = (TextView) findViewById(R.id.login);
        register = (TextView) findViewById(R.id.register);
        forgot = (TextView) findViewById(R.id.forgot);
        login.setOnClickListener(this);
        register.setOnClickListener(this);
        forgot.setOnClickListener(this);
	}


	@Override
	public void onClick(View v) {
		if (v.getId()== R.id.login) {
            UtilUser.login(UserText.getText().toString(), PassText.getText().toString(), getApplication(), new RestCallback<Boolean>() {
                @Override
                public void success(Boolean aBoolean, Response response) {
                    authenticateUser();
                }

                @Override
                public void failure(RestError error) {
                    Toast.makeText(getApplicationContext(),error.getStrMessage(),Toast.LENGTH_SHORT).show();
                }
            });
		}
        if (v.getId()== R.id.register) {
            Intent regi = new Intent(this,RegisterActivity.class);
            startActivity(regi);
            LogInPageActivity.this.finish();
        }
        if (v.getId()== R.id.forgot) {
            finish();
        }
	}


    private void authenticateUser(){
        if(UtilUser.isAuthenticated()){
            finish();
        }
    }
}