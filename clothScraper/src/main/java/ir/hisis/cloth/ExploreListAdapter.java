package ir.hisis.cloth;

import ir.hisis.cloth.CategoryGroup.Category;

import java.util.ArrayList;
import java.util.zip.Inflater;

import android.annotation.SuppressLint;
import android.content.Context;

import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ExploreListAdapter extends BaseExpandableListAdapter {
	
	
	private final String TAG = "hisisExploreAdabter";
	static class ViewHolder {
	    public TextView text;
	    public TextView count;
	    public ImageView image;
	    
	  }

	ArrayList<CategoryGroup> groups = new ArrayList<CategoryGroup>();
	private final FragmentActivity context ;
	
	public ExploreListAdapter(FragmentActivity context){
		this.context = context;
		
		initiate();
		
	}
	@Override
	public Object getChild(int arg0, int arg1) {
		return groups.get(arg0).getCategory(arg1);
	}

	@Override
	public long getChildId(int arg0, int arg1) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView,
			ViewGroup parent) {
		View rowView = convertView;
		if(rowView==null){
			LayoutInflater inflater = context.getLayoutInflater();
			rowView = inflater.inflate(R.layout.explore_list_item, null);
			ViewHolder viewHolder = new ViewHolder();
			viewHolder.text = (TextView) rowView.findViewById(R.id.txtCategoryName);
			viewHolder.count = (TextView) rowView.findViewById(R.id.txtCategoryCount);
			rowView.setTag(viewHolder);
			
			rowView.setOnClickListener(new OnClickListener() {
				
				
				@Override
				public void onClick(View v) {
					ViewHolder holder = (ViewHolder) v.getTag();
					CategoryFragment cf = CategoryFragment.newInstance( (String) holder.text.getText(), false );
					context.getSupportFragmentManager().beginTransaction().replace(R.id.frmExplore, cf).commit();
				}
			});
		}
		
		ViewHolder holder = (ViewHolder) rowView.getTag();
		holder.text.setText(groups.get(groupPosition).getCategory(childPosition).getName());
		holder.count.setText(String.valueOf(groups.get(groupPosition).getCategory(childPosition).getCategoryCount()));
		
		return rowView;
	}

	@Override
	public int getChildrenCount(int arg0) {
		
		return groups.get(arg0).getchildrenCount();
	}

	@Override
	public Object getGroup(int arg0) {
		return groups.get(arg0);
	}

	@Override
	public int getGroupCount() {
		return groups.size();
	}

	@Override
	public long getGroupId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isLastChild, View converView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View rowView = converView;
		
		if(rowView == null){
			LayoutInflater inflater = context.getLayoutInflater();
			rowView = inflater.inflate(R.layout.explore_group_item, null);
			ViewHolder viewHolder = new ViewHolder();
			viewHolder.text = (TextView) rowView.findViewById(R.id.txtvGroupName);

			rowView.setTag(viewHolder);
		}
		
		ViewHolder holder = (ViewHolder) rowView.getTag();
		holder.text.setText(groups.get(groupPosition).getName());

		return rowView;
	}

	@Override
	public boolean hasStableIds() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isChildSelectable(int arg0, int arg1) {
		// TODO Auto-generated method stub
		return false;
	}

	private void initiate(){
		for(int i=0; i<5; i++){
			CategoryGroup group = new CategoryGroup();
			group.setName("gourp" + String.valueOf(i));
			for (int j=0; j<4; j++){
				Category category = new Category();
				category.setName("category" + String.valueOf(j));
				category.setCategoryCount(10*i + 3*j);
				group.categories.add(category);
			}
			
			groups.add(group);
		}
	}
}
