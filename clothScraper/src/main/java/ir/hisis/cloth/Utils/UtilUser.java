package ir.hisis.cloth.Utils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

import ir.hisis.cloth.rest.RestCallback;
import ir.hisis.cloth.rest.RestClient;
import ir.hisis.cloth.rest.model.Message;
import ir.hisis.cloth.rest.model.Register;
import ir.hisis.cloth.rest.model.RestError;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Header;
import retrofit.client.Response;

public class UtilUser {
    private static String TAG = UtilUser.class.toString();
    private static String AUTH_TOKEN = "";
    private static String AUTH_TOKEN_LABEL = "Authorization";
	public static String USER_NAME_LABEL = "user_name";
	public static String PASSWORD_LABEL = "password";
	public static String AUTHENTICATIONFILE = "authentication";

	
	public static class PasswordMatchException extends Exception{
	}
	
	public static boolean isAuthenticated(){
        Log.i(TAG, "Auth token is: " + AUTH_TOKEN);
        if (UtilValidate.isNotEmpty(AUTH_TOKEN))
            return true;
        else
            return false;
	}
    public static void loginWithSharePreferences(Application application, RestCallback<Boolean> callback){
        Map<String, String> token = getAuthToken(application);
        login(token.get(USER_NAME_LABEL), token.get(PASSWORD_LABEL), application, callback);
    }

    public static Map<String, String> getAuthToken(Application application){
        SharedPreferences sharedPreferences = application.getSharedPreferences(AUTHENTICATIONFILE, Context.MODE_PRIVATE);
        Map<String, String> token  = new HashMap<String, String>();
        token.put(USER_NAME_LABEL, sharedPreferences.getString(USER_NAME_LABEL, null));
        token.put(PASSWORD_LABEL, sharedPreferences.getString(PASSWORD_LABEL, null));
        token.put(AUTH_TOKEN_LABEL, sharedPreferences.getString(AUTH_TOKEN_LABEL,null));
        return token;
    }

	public static void login(String userName, String passwd, final Application application, final RestCallback<Boolean> callback){
        RestClient rc = new RestClient();
        final Register register = new Register(userName, passwd);
        rc.getClothServer().login(register, new RestCallback<Message>() {
            @Override
            public void failure(RestError restError) {
                Log.i(TAG, "Error mesage: " + restError.getStrMessage() + " code: " + restError.getCode());

            }

            @Override
            public void success(Message message, Response response) {
                if (UtilValidate.isNotEmpty(getAuthToken(response))) {
                    Log.i(TAG,"in authenticated");
                    makeUserAuthenticated(register, getAuthToken(response), application);
                    callback.success(true, response);
                }else{
                    Log.i(TAG,"in not authenticated ");
                    callback.failure(new RestError(message.getMessage()));
                }
            }
        });
	}


    public static void registerUser(String userName, String passwd, String repeatPass, Application application, RestCallback<Boolean> callback)throws PasswordMatchException{
        if(!passwd.equals(repeatPass)) throw new PasswordMatchException();
        doRegister(userName, passwd, application, callback);
    }

    public static void doRegister(final String userName, final String passwd, final Application application, final RestCallback<Boolean> callback){
        final Register register = new Register( userName, passwd);
        RestClient rc = new RestClient();
        rc.getClothServer().register(new Register( userName, passwd), new Callback<Message>() {
            @Override
            public void success(Message message, Response response) {
                if(UtilValidate.isNotEmpty(getAuthToken(response))) {
                    UtilUser.makeUserAuthenticated(register, getAuthToken(response), application);
                    callback.success(Boolean.TRUE, response);
                }else{
                    callback.failure(new RestError(message.getMessage()));
                }
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }


    public static void makeUserAuthenticated(Register register, String token, Application application ){
        saveUserToSharedPreferences(register, token, application);
        AUTH_TOKEN = token;
    }

    private static void saveUserToSharedPreferences(Register register, String token, Application application) {
        SharedPreferences sharedPreferences =  application.getSharedPreferences(AUTHENTICATIONFILE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(USER_NAME_LABEL, register.getUserName());
        editor.putString(PASSWORD_LABEL, register.getPassword());
        editor.putString(AUTH_TOKEN_LABEL, token);
        editor.commit();
    }

    public static boolean isRegistered(Application application){
        if(UtilValidate.isNotEmpty(AUTH_TOKEN))
            return true;
        Map<String, String> token = getAuthToken(application);
        if(UtilValidate.isEmpty(token.get(USER_NAME_LABEL))|| UtilValidate.isEmpty(token.get(PASSWORD_LABEL)))
            return false;
        return true;
    }


    public static String getAuthToken(Response response){
        List<Header> headerList = response.getHeaders();

        for(int i=0; i<headerList.size(); i++){
            if(headerList.get(i).getName().startsWith(AUTH_TOKEN_LABEL))
                return headerList.get(i).getValue();
        }
        return "";
    }

}
