package ir.hisis.cloth;

/**
 * Created by pooyafayyaz on 4/26/2015.
 */

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.etsy.android.grid.StaggeredGridView;

public  class MainFragment extends Fragment {

    public MainFragment() {

    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_main, container,
                false);
        return rootView;
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        StaggeredGridView gridView = (StaggeredGridView) getView().findViewById(R.id.grid_view);
        gridView.setAdapter(new ImageAdapter(getActivity()));

    }
}

