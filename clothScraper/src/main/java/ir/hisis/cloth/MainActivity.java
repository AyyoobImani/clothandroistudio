package ir.hisis.cloth;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.support.v4.app.DialogFragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import ir.hisis.cloth.Utils.UtilUser;
import ir.hisis.cloth.rest.RestCallback;
import ir.hisis.cloth.rest.model.RestError;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MainActivity extends ActionBarActivity implements ActionBar.TabListener {

    private TabsPagerAdapter mAdapter;
    private ViewPager viewPager;
    private ActionBar actionBar;
    private static String TAG = MainActivity.class.toString();

    String[] tabs = { "main", "favorite", "tops" };


	@Override
	protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, "before setting content view");
        setContentView(R.layout.activity_main);
        Log.i(TAG, "set content view");
        if(!UtilUser.isAuthenticated())
            if(UtilUser.isRegistered(getApplication()))
                tryToLogin(savedInstanceState);
        setupUI();
	}

    private void tryToLogin(final Bundle savedInstanceState){
        UtilUser.loginWithSharePreferences(getApplication(), new RestCallback<Boolean>() {
            @Override
            public void success(Boolean aBoolean, Response response) {
            }

            @Override
            public void failure(RestError error) {
            }
        });
    }



    private void setupUI(){
        Log.i(TAG, "setting up ui");
        setContentView(R.layout.activity_main);
        setTitle("");
        viewPager = (ViewPager) findViewById(R.id.container);
        actionBar = getSupportActionBar();
        mAdapter = new TabsPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(mAdapter);
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        for (String tab_name : tabs) {
            actionBar.addTab(actionBar.newTab().setText(tab_name)
                    .setTabListener(this));
        }

        setupViewpagersOnchangeListener();
    }

    private void setupViewpagersOnchangeListener() {
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                actionBar.setSelectedNavigationItem(position);
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
            }

            @Override
            public void onPageScrollStateChanged(int arg0) {
            }
        });
    }


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		switch(id){
			case(R.id.action_explore):
				Intent intent = new Intent(getApplicationContext(), ExpandableListViewActivity.class);
				startActivity(intent);
				return true;
			case(R.id.action_settings):
				Intent intent2 = new Intent(getApplicationContext(), CategoryActivity.class);
				startActivity(intent2);
				return true;
			}
		return super.onOptionsItemSelected(item);
		 
	}


    @Override
    public void onTabSelected(ActionBar.Tab tab, android.support.v4.app.FragmentTransaction fragmentTransaction) {
        if(tab.getPosition() == 1 && !UtilUser.isRegistered(getApplication()))
                new registerDialog().show(getSupportFragmentManager(),"tagesh");
        else
            viewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, android.support.v4.app.FragmentTransaction fragmentTransaction) {

    }
    @Override
    public void onTabReselected(ActionBar.Tab tab, android.support.v4.app.FragmentTransaction fragmentTransaction) {

    }

    public static class registerDialog extends DialogFragment {
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            setupDialogParameters(builder);
            return builder.create();
        }

        private void setupDialogParameters(AlertDialog.Builder builder) {
            builder.setMessage(R.string.you_should_login_to_access_this)
                    .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            Intent intent = new Intent(getActivity().getApplicationContext(), LogInPageActivity.class);
                            startActivity(intent);
                        }
                    })
                    .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                        }
                    });
        }

    }
}
